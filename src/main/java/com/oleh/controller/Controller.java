package com.oleh.controller;

import com.oleh.model.Model;

public class Controller implements ControllerInt {

    private Model model;

    public Controller(){
        model = new Model();
    }

    public void startApp() {
        System.out.println("app starting");
        model.makeObj();
    }
}
