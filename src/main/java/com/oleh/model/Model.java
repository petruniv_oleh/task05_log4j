package com.oleh.model;

import com.oleh.model.cars.Car;
import com.oleh.model.cars.Lamborgini;
import com.oleh.model.cars.Skoda;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Model implements ModelInt {
    private static Logger logger = LogManager.getLogger(Model.class.getName());


    public void makeObj() {
        List<Car> cars = new ArrayList<Car>();

        cars.add(new Car(150, 5000));
        cars.add(new Skoda(200, 15000, true, false));
        cars.add(new Lamborgini(350, 200000, 8, 3.1));
        logger.debug("debug");
        logger.trace("trace");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
        logger.fatal("fatal");

    }


}
