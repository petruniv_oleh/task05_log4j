package com.oleh.model.cars;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Car {
    private int maxSpeed;
    private int price;

    private final static Logger logger = LogManager.getLogger(Car.class.getName());

    public Car() {
    }

    public Car(int maxSpeed, int price) {
        logger.debug("debug");
        logger.trace("trace");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
        logger.fatal("fatal");
        this.maxSpeed = maxSpeed;
        this.price = price;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void move(){

        System.out.println("slowly moving");
    }

    @Override
    public String toString() {
        return "Car{" +
                "maxSpeed=" + maxSpeed +
                ", price=" + price +
                '}';
    }
}
