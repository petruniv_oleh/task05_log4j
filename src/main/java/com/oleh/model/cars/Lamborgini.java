package com.oleh.model.cars;

public class Lamborgini extends Car {
    private int amountOfPistons;
    private double fromZeroToHundred;

    public Lamborgini() {
    }

    public Lamborgini(int amountOfPistons, double fromZeroToHundred) {
        this.amountOfPistons = amountOfPistons;
        this.fromZeroToHundred = fromZeroToHundred;
    }

    public Lamborgini(int maxSpeed, int price, int amountOfPistons, double fromZeroToHundred) {
        super(maxSpeed, price);
        this.amountOfPistons = amountOfPistons;
        this.fromZeroToHundred = fromZeroToHundred;
    }

    public int getAmountOfPistons() {
        return amountOfPistons;
    }

    public void setAmountOfPistons(int amountOfPistons) {
        this.amountOfPistons = amountOfPistons;
    }

    public double getFromZeroToHundred() {
        return fromZeroToHundred;
    }

    public void setFromZeroToHundred(double fromZeroToHundred) {
        this.fromZeroToHundred = fromZeroToHundred;
    }

    @Override
    public void move() {
        System.out.println("rrrrr rrrr rrrrr");
        System.out.println("moving very fast");
    }

    @Override
    public String toString() {
        return "Lamborgini{" + super.toString()+
                ", amountOfPistons=" + amountOfPistons +
                ", fromZeroToHundred=" + fromZeroToHundred +
                '}';
    }
}
