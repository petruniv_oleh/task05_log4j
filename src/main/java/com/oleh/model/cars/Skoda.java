package com.oleh.model.cars;

public class Skoda extends Car {
    private boolean computer;
    private boolean seatHeat;

    public Skoda() {
    }

    public Skoda(boolean computer, boolean seatHeat) {
        this.computer = computer;
        this.seatHeat = seatHeat;
    }

    public Skoda(int maxSpeed, int price, boolean computer, boolean seatHeat) {
        super(maxSpeed, price);
        this.computer = computer;
        this.seatHeat = seatHeat;
    }

    public boolean isComputer() {
        return computer;
    }

    public void setComputer(boolean computer) {
        this.computer = computer;
    }

    public boolean isSeatHeat() {
        return seatHeat;
    }

    public void setSeatHeat(boolean seatHeat) {
        this.seatHeat = seatHeat;
    }

    @Override
    public void move() {

        System.out.println("smoothly moving");
    }

    @Override
    public String toString() {
        return "Skoda{" + super.toString()+
                ", computer=" + computer +
                ", seatHeat=" + seatHeat +
                '}';
    }
}
