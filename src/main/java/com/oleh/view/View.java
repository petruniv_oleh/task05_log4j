package com.oleh.view;

import com.oleh.controller.Controller;

import java.nio.charset.Charset;
import java.util.Scanner;

public class View implements VievInt {
    Controller controller;

    public View() {
        controller = new Controller();
        menu();
    }

    public void menu() {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Press 1 to start the app");
        if (sc.nextInt()==1) {
            controller.startApp();
        }else{
            System.out.println("You entered a wrong number");
            System.out.println("Closing");
        }
    }
}
